﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private Animator PlayerAnimController;

  


    public static float distanceTraveled;

    public float acceleration;
    public Vector3 jumpVelocity;
    public float timer;
    ScoreScript TheScript;
    GameObject Score;


    private bool touchingPlatform; //used to determine if the rigidbody of the player is touching a platform

    void Start()
    {
        PlayerAnimController.SetBool("IsFalling", false);

        Score = GameObject.Find("Text"); //finds the game object Text
        TheScript = Score.GetComponent<ScoreScript>(); //finds the script on Text
    }

   

    void Update()
    {
        
        if(touchingPlatform == true)
        {
            timer = .8f; //sets timer for fall animation
            PlayerAnimController.SetBool("IsFalling", false);
            if (Input.GetButtonDown("Jump"))
            {
                PlayerAnimController.SetBool("IsJumping", true);
                GetComponent<Rigidbody>().AddForce(jumpVelocity, ForceMode.VelocityChange);
               
            }
            PlayerAnimController.SetBool("IsRunning",true);
        }
        if(touchingPlatform == false)
        {
            timer -= Time.deltaTime;
            if (timer < 0)//checks if timer is at 0
            {
                PlayerAnimController.SetBool("IsFalling", true);
            }

            //if (transform.position.y < -1)
            // {
            // PlayerAnimController.SetBool("IsFalling", true);
            //}
        }

        
    }


    void FixedUpdate()
    {
        if (touchingPlatform)
        {
            if (distanceTraveled < 400f)
            {
                GetComponent<Rigidbody>().AddForce(acceleration, 0f, 0f, ForceMode.Acceleration); //if the rigidbody is touching a platform, then there is a force
                                                                                                  //being added via the acceleration float which causes the player to endlessly run
            }

        }
    }


    void OnCollisionEnter()
    {
        Debug.Log("runningNow");
        touchingPlatform = true; //if the player's rigidbody collides with the platform, set touchingPlatform to true
        PlayerAnimController.SetBool("IsJumping", false);
        PlayerAnimController.SetBool("IsRunning", true);
      


    }

    void OnCollisionExit()
    {
        touchingPlatform = false; //if the player's rigidbody is no longer in contact with the platform, set touchingPlatform to false
        
    }

    private void OnTriggerEnter(Collider other) 
    {
        if (other.gameObject.CompareTag("pickup"))
        {
            TheScript.score += 100f;//supposed to add 100 to the score variable in ScoreScript
            other.gameObject.SetActive(false); //gameobject will be removed if player character contacts the gameobject and it is tagged "pickup"
        }
    }
   
}
