﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public void PlayGame ()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1); //goes to game when the player clicks on the play game button at the start screen
    }

    public void QuitGame ()
    {
        
        Application.Quit(); //quits game when player clicks on exit game button
        
    }

    public void MainMenu()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 2); //goes to main menu when player clicks play again at the game over screen
    }
}
