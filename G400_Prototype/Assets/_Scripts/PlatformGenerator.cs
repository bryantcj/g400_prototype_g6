﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformGenerator : MonoBehaviour
{
    public GameObject thePlatform;
    public Transform generationPoint;
    public float distanceBetween;

    private float platformWidth;

   
    void Start()
    {
        //defining width of the platform is same with width of the box collider
        platformWidth = thePlatform.GetComponent<BoxCollider>().size.x;
    }

    
    void Update()
    {
        //if the x position of generator is less than x position of genration points it spawn a platform
        if (transform.position.x < generationPoint.position.x)
        {
            //moving the genrator to new x position 
            transform.position = new Vector3(transform.position.x + platformWidth + distanceBetween, transform.position.y, transform.position.z);
            // spawning the platform prefab at generator after moving to new x position
            Instantiate(thePlatform, transform.position, transform.rotation);
        }
    }
}
