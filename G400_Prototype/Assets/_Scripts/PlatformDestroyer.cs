﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformDestroyer : MonoBehaviour
{
    public GameObject platformDestructionPoint;

    
    void Start()
    {
        //when game start find the destruction point automatically and set as platformDestructionPoint
        platformDestructionPoint = GameObject.Find("PlatformDestructionPoint");
    }

    
    void Update()
    {
        //if platform x posiotion is smaller than x position of destruction point, destroy the platform
        if (transform.position.x < platformDestructionPoint.transform.position.x)
        {
            Destroy(gameObject);
        }
    }
}
